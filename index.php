<?php 
require('class/cleverConnect.class.php');
require('class/cleverSearch.class.php');

include('inc/head.inc.php');

	/* ---------------------------------------------------- */
	//Chargement des données
	/* ---------------------------------------------------- */
	if(isset($_GET['def_tok']) && isset($_GET['def_tok_s']))
	{
		$_SESSION['cleverUser']['acess_token'] = $_GET['def_tok'];
		$_SESSION['cleverUser']['acess_token_secret'] = $_GET['def_tok_s'];
		$_SESSION['cleverUser']['user_name'] = $_GET['name'];
		$_SESSION['cleverUser']['profilpic'] = $_GET['profilpic'];
	}
	
	$cleverSearch = new cleverSearch();	
	//affichage search
	if(isset($_POST['search']) && !isset($_POST['search']))
	{$_SESSION['cleverUser']['save_search'] = stripslashes($_POST['search']);}
	elseif(isset($_REQUEST['s']) && !empty($_REQUEST['s']))
	{$_SESSION['cleverUser']['save_search'] = $_REQUEST['s'];}
	else {$_SESSION['cleverUser']['save_search'] = $cleverSearch->toTrends();}
	
	/* ---------------------------------------------------- */
	//Création de la connexion
	/* ---------------------------------------------------- */
	$cleverConnect = new cleverConnect();
	if(!isset($_SESSION['cleverUser']['acess_token']))
	{
		$to_url = $cleverConnect->toConnect();
		?>
        <a href="<?php echo $to_url; ?>" id="connexion"></a>
        <?php	
		include('inc/intro.inc.php');	
	}
	
	/* ---------------------------------------------------- */
	//Si l'ensemble des variables user son définit
	/* ---------------------------------------------------- */
	if(isset($_SESSION['cleverUser']['user_name']) && $_GET['p'] != 'w')
	{
		?>
        <div id="compte">
        <img alt="<?php echo $_SESSION['cleverUser']['user_name']; ?>" src="
		   <?php echo $cleverConnect->toProfilpic($_SESSION['cleverUser']['acess_token'], $_SESSION['cleverUser']['acess_token_secret'], $_SESSION['cleverUser']['user_name']); ?>" />
           <a class="button" id="close" href="<?php echo $_SERVER['PHP_SELF']; ?>?p=d"  ><img src="images/close.png" alt="deconnexion" /></a>
           <a class="button" id="identifiant" title="<?php echo $_SESSION['cleverUser']['user_name']; ?>" href="https://twitter.com/#!/<?php echo $_SESSION['cleverUser']['user_name']; ?>"><?php echo $_SESSION['cleverUser']['user_name']; ?></a>
           
        </div>
        <?php
		
		include('inc/main.inc.php');
	}
	elseif(isset($_GET['p'])&& $_GET['p'] == 'w')
	{include('inc/who.inc.php'); }
	else{include('inc/intro.inc.php');}
	

include('inc/footer.inc.php');

?>
