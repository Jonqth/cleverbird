<?php
//////////////////////////////////////////////
//CleverBird
//Auth : Araujo-Levy Jonathan
//copyright : COESENSE.COM
//2011 - 2012
//////////////////////////////////////////////

class cleverTreat
{
	public function __construct()
	{}
	
	function in_multi_array($value, $array)
	{    
		foreach ($array as $key => $item)
		{        
			if (!is_array($item))
			{if ($item == $value) return true;}
			else{
				if ($key == $value) return true;
				if (in_array($value, $item)) return true;
				else if ($this->in_multi_array($value, $item)) return true;
			}
		}
		return false;
	}

	
	//rangement
	public function toTide($hash)
	{
		$hashtags = array();
		$hashs = array();
		if($this->in_multi_array($hash,$hashtags)){
			$hashs["size"] = $hashs["size"]+1;
		}
		else{
			$rand_p = mt_rand(50, 90);
			$rand_q = mt_rand(50, 90);
			$randX = mt_rand(3,$rand_p);
			$randY = mt_rand(3,$rand_q);
			$hashs = array("text" => $hash ,"size" => 0, "posX" => $randX, "posY" => $randY);
			array_push($hashtags, $hashs);
		}
		return $hashtags;
	}

}


?>