<?php
//////////////////////////////////////////////
//CleverBird
//Auth : Araujo-Levy Jonathan
//copyright : COESENSE.COM
//2011 - 2012
//////////////////////////////////////////////

require('inc/vars.inc.php');
require('lib/twittersearch.php');
require('class/cleverTreat.class.php');

class cleverSearch
{
	public function __construct()
	{}
	
	//regEX d'highlights des hTags et mentions et liens
	public function linkify_tweet($tweet)
	{
	  $tweet = preg_replace('/(https?:\/\/\S+)/','<a href="\1">\1</a>',$tweet);
	  $tweet = preg_replace('/(^|\s)@(\w+)/','\1<a href="http://twitter.com/\2">@\2</a>',$tweet);
	  $tweet = preg_replace('/(^|\s)#(\w+)/','\1<a href="http://cleverbird.coesense.com/index.php?p=r&s=%23\2">#\2</a>',$tweet);
	  return $tweet;
	}
	
	//Parse les hashtags
	public function analyse_hash($search)
	{
		$hashs = preg_match_all("/(#\w+)/", $search, $matches);
		return $matches[1];
	}
	
	//recherche trends
	public function toTrends()
	{
		$cleverSearch = new TwitterSearch();
		$trends = $cleverSearch->trends();
		foreach($trends as $trend){
			return $trend->trends[0]->name;
		}
	}
	
	//recherche
	public function toSearch($search = array())
	{
		$search_length = count($search);
		for($i=0; $i<$search_length; $i++)
		{
			$cleverSearch = new TwitterSearch($search[$i]);
			$hashtages = $cleverSearch->rpp(30)->incent(true)->results();
			
			foreach($hashtages as $hashtags){
				$hashs = "#"; 
				$hashs.= $hashtags->entities->hashtags[0]->text;
				$cleverTreat = new cleverTreat();
				$final_hashs = $cleverTreat->toTide($hashs);
				$final_hashs_length = count($final_hashs);
				for($i=0; $i<$final_hashs_length; $i++){
					if($final_hashs[$i]["text"] == $search[$i]){}
					else{
					?>
					<div style="<?php echo "top:".$final_hashs[$i]['posY']."%; left:".$final_hashs[$i]['posX']."%;"; ?>" class="bulle">
							<a href="index.php?p=r&s=%23<?php echo preg_replace('`#`','',$final_hashs[$i]["text"]); ?>">
								<?php 
									echo $final_hashs[$i]["text"];
								?>         
							</a>
					</div>
					<?
					}
				}
			}
		}
		
	}
	
	//recherche rightside
	public function toTimeSearch($user,$search = array())
	{
		$search_length = count($search);
		for($i=0; $i<$search_length; $i++)
		{
			$cleverSearch = new TwitterSearch($search[$i]);
			$resultses = $cleverSearch->rpp(150)->results();

			foreach($resultses as $results){
				?>
					<li>
						<img alt="" style="display:inline-block; height:35px;" src="<?php echo $results->profile_image_url; ?>"/>
						<p> 
							<a href="https://twitter.com/#!/<?php echo $results->from_user; ?>"><?php echo "@".$results->from_user; ?></a>
							<?php echo $this->linkify_tweet($results->text); ?>
						</p>
					</li>
				<?php
			}
		}
	}
}
?>