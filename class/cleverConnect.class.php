<?php
//////////////////////////////////////////////
//CleverBird
//Auth : Araujo-Levy Jonathan
//copyright : COESENSE.COM
//2011 - 2012
//////////////////////////////////////////////

require('inc/vars.inc.php');
require('lib/twitteroauth.php');

class cleverConnect
{
	
	public function __construct()
	{}
	
	/* ====================================================================== */
	// Générales
	/* ====================================================================== */
	//Demande de connexion
	public function toConnect()
	{
		$cleverConnection = new TwitterOAuth('zDBEukzQLuMXUp7v0NY0vw', '2hdsz3I0kVJs6HsPgU0CbVW43hYl9h7XZ4gQYI');
		$request_token = $cleverConnection->getRequestToken();
		
		//Stockage en session des premier token retour
		$_SESSION['oauth_token'] = $request_token['oauth_token'];
		$_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];
		
		//obtenir l'url d'authorisation
		$to_url = $cleverConnection->getAuthorizeURL($request_token['oauth_token']);
		
		//header('Location :'.$to_url);
		return $to_url;
	}
	/* ====================================================================== */
	// Générales
	/* ====================================================================== */
	
	/* ====================================================================== */
	// Fonctions inhérantes
	/* ====================================================================== */
	
	//Déconnexion
	public function toDeconnect()
	{
		unset($_SESSION['cleverUser']['user_name']);
		unset($_SESSION['cleverUser']['acess_token']);
		unset($_SESSION['cleverUser']['acess_token_secret']);
		unset($_SESSION['cleverUser']['user_name']);
		unset($_SESSION['cleverUser']['profilpic']);
		session_destroy();
		header("refresh:0; url='index.php'");
	}
	
	//regEX d'highlights des hTags et mentions et liens
	public function linkify_tweet($tweet)
	{
	  $tweet = preg_replace('/(https?:\/\/\S+)/','<a href="\1">\1</a>',$tweet);
	  $tweet = preg_replace('/(^|\s)@(\w+)/','\1<a href="http://twitter.com/\2">@\2</a>',$tweet);
	  $tweet = preg_replace('/(^|\s)#(\w+)/','\1<a href="http://cleverbird.coesense.com/index.php?p=r&s=%23\2">#\2</a>',$tweet);
	  return $tweet;
	}
	
	/* ====================================================================== */
	// Fonctions inhérantes
	/* ====================================================================== */
	
	/* ====================================================================== */
	// Données user
	/* ====================================================================== */
	
	//récupération de l'image
	public function toProfilpic($def_tok,$def_tok_s,$user_name)
	{
		$cleverConnection = new TwitterOAuth('zDBEukzQLuMXUp7v0NY0vw', '2hdsz3I0kVJs6HsPgU0CbVW43hYl9h7XZ4gQYI', $def_tok, $def_tok_s);
		$profilpics = $cleverConnection->get('users/show/'.$user_name);
		return $profilpics->profile_image_url;
	}
	
	//récupère la timeline
	public function toTimeline($def_tok,$def_tok_s)
	{	
		$cleverConnection = new TwitterOAuth('zDBEukzQLuMXUp7v0NY0vw', '2hdsz3I0kVJs6HsPgU0CbVW43hYl9h7XZ4gQYI', $def_tok, $def_tok_s);
		$statuses = $cleverConnection->get('statuses/home_timeline');
		
		foreach($statuses as $status){
            ?>
                <li>
					<img style="display:inline-block; height:35px;" alt="" src="<?php echo $status->user->profile_image_url; ?>"/>
					<p>
                    	<a href="https://twitter.com/#!/<?php echo $status->user->screen_name; ?>"><?php echo "@".$status->user->screen_name; ?></a>
						<?php echo $this->linkify_tweet($status->text); ?>
                    </p>
				</li>
            <?php
        }
		
	}
	
	//récupère les mentions
	public function toMentions($def_tok,$def_tok_s)
	{
		$cleverConnection = new TwitterOAuth('zDBEukzQLuMXUp7v0NY0vw', '2hdsz3I0kVJs6HsPgU0CbVW43hYl9h7XZ4gQYI', $def_tok, $def_tok_s);
		$mentions = $cleverConnection->get('statuses/mentions');
		
		foreach($mentions as $mention){
			?>
                <li>
					<img alt="" style="display:inline-block; height:35px;" src="<?php echo $mention->user->profile_image_url; ?>"/>
                    <p> 
                        <a href="https://twitter.com/#!/<?php echo $mention->user->screen_name; ?>"><?php echo "@".$mention->user->screen_name; ?></a>
                        <?php echo $this->linkify_tweet($mention->text); ?>
                    </p>
				</li>
            <?php
        }
		
	}
	
	//Pour tweeter
	public function toTweet($def_tok,$def_tok_s,$tweet)
	{
		$cleverConnection = new TwitterOAuth('zDBEukzQLuMXUp7v0NY0vw', '2hdsz3I0kVJs6HsPgU0CbVW43hYl9h7XZ4gQYI', $def_tok, $def_tok_s);
		$tweet = stripslashes($tweet);
		$cleverConnection->post('statuses/update',array('status'=>$tweet));
	}
	
	/* ====================================================================== */
	// Données user
	/* ====================================================================== */

	
}


?>