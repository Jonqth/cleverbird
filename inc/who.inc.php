</header>
<section id="page">
		<div id="content">
			<h2>Qui sommes nous ?</h2>
			<article>
				<h3><span>1</span> Un concept novateur</h3>
				<p>
					Notre concept se base sur les hashtags comme catégories d'un tweet. Différents hashtags sont souvent utiliser dans un seul et même tweet.
				</p>
				<p>
					Notre service permet d'optimiser son Tweet. Le procédé est simple : comme pour un moteur de recherche classique, l'infinité des informations est "triée" par mot clé. Il est donc question d'offrir à l'utilisateur une visualisation des hashtags les plus associés à un hashtag ou mot clé particulier.
				</p>
			</article>
			<article>
				<h3><span>2</span> Le partenaire de votre entreprise</h3>
				<p>
					 Notre modèle possède une forte valeur ajoutée puisque nous pouvons garantir une meilleure pertinence dans l'utilisation de vos tweets vous assurant ainsi une meilleure visibilité au sein du réseau social.
				</p>
			</article>
			<article>
				<h3><span>3</span> Mentions légales</h3>
				<p>
					Conformément à la loi « informatique et libertés » du 6 janvier 1978 modifiée en 2004, vous bénéficiez d'un droit d'accès et de rectification aux informations qui vous concernent, que vous pouvez exercer en vous adressant à cleverbird@cleverbird.com
				</p>
				<p>
					Vous pouvez également, pour des motifs légitimes, vous opposer au traitement des données vous concernant.
				</p>
			</article>
			<article>
				<h3><span>4</span> Droit d'auteur</h3>
				<p>
					Ce site respecte le droit d'auteur. Tous les droits des auteurs des Oeuvres protégées reproduites et communiquées sur ce site, sont réservés. Sauf autorisation, toute utilisation des Oeuvres autres que la reproduction et la consultation individuelles et privées sont interdites
				</p>
			</article>
		</div>
		<div id="parallax" class="background">
			<div id="target">			
				<div style="width:1200px; height:640px;">
					<div class="ashtag" style="position:absolute; top:-400px; left:-100px; color:#9bc0e5">
						<div class="bulle" style="background:#c1c1c1;border:20px solid #d8d8d8;width:600px;height:600px;"></div>
				</div>
				<div style="width:1800px; height:420px;">
					<div class="ashtag" style="position:absolute; top:70%; left:-50px; color:#a4c522">
						<div class="bulle" style="background:#9e9c9c;border:6px solid #afafaf;width:200px;height:200px;"></div>
					</div>
				  </div>
				<div style="width:1400px; height:460px;">
					<div class="ashtag" style="position:absolute; top:20%; left:80%; color:#e99400">
						<div class="bulle" style="background:#808080;border:10px solid #9d9d9d;width:170px;height:170px;"></div>
				</div>  
				<div style="width:2000px; height:495px;">
					<div class="ashtag" style="position:absolute; top:10%; left:96%; color:#b50c0c;">
						<div class="bulle" style="background:#c1c1c1;border:4px solid #d8d8d8;width:60px;height:60px;"></div>
					</div>
				</div>  
				<div style="width:1540px; height:480px;">
					<div class="ashtag" style="position:absolute; top:60%; left:20%; color:#e99400;">
						<div class="bulle" style="background:#9d9d9d;border:5px solid #bdbdbd;width:90px;height:90px;"></div>
					</div>
					<div class="ashtag" style="position:absolute; top:40%; left:10%; color:#b50c0c;">
						<div class="bulle" style="background:#8c8a8a;border:3px solid #abaaaa;width:30px;height:30px;"></div>
					</div>
				</div>
				<div style="width:1200px; height:640px;">
					<div class="ashtag" style="position:absolute; top:77%; left:50%; color:#9bc0e5">
						<div class="bulle" style="background:#c1c1c1;border:33px solid #d8d8d8;width:666px;height:666px;"></div>
				</div>				
			</div>
		</div>
	</section>