</header>
	<section id="board">
		<nav>
			<form name="cleverbird-tr_form" method="post" enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF']; ?>">
				<textarea name="search" maxlength="140" rows="3" cols="45"><?php if(isset($_POST['search']) && !empty($_POST['search'])){ echo $_SESSION['cleverUser']['save_search'] = stripslashes($_POST['search']); }else{ echo $_SESSION['cleverUser']['save_search'];}?></textarea>
				<input class="button" type="submit" id="rechercher" value="Rechercher" />
				<input class="button" type="submit" id="tweeter" value="Tweeter" />
			</form>
            <?php 
				if(isset($_POST['tweet'])){
					$tweet = $_POST['tweet'];
					$cleverConnect->toTweet($_SESSION['cleverUser']['acess_token'], $_SESSION['cleverUser']['acess_token_secret'], $tweet);
					echo "<span style='text-align:center; color:#fff; display:block; margin-bottom:10px; font-weight:bold; font-size:0.7em;'>Votre tweet à bien été envoyé</span>";
				}
			?>      
		</nav>
		<div id="bulle" class="bulle">
			<a class="title" href="#">
				<?php 
				if(isset($_POST['search']) && !empty($_POST['search'])) {
					$hashs = $cleverSearch->analyse_hash($_POST['search']);
					foreach($hashs as $hash){
						echo $hash."\n\r";
					}
				}
				elseif(isset($_REQUEST['s']) && !empty($_REQUEST['s']))
				{ echo $_REQUEST['s']; }
				else {
					echo $_SESSION['cleverUser']['save_search'];
				}
				?>
           </a>
		</div>
       <div id="parallax" class="background">
			<div id="target">			
				<div style="width:1200px; height:640px;">
                    <div class="ashtag">
                        <?php
                            if(isset($_POST['search']) && !empty($_POST['search'])) 
                            {$cleverSearch->toSearch($hashs);}
                            elseif(isset($_REQUEST['s']) && !empty($_REQUEST['s']))
                            {
                                $search_clv = array();
                                $search_clv[0] = $_REQUEST['s'];
                                $cleverSearch->toSearch($search_clv);
                            }
                            else{ 
                                $search_clv = array();
                                $search_clv[0] = $cleverSearch->toTrends();
                                $cleverSearch->toSearch($search_clv); 
                            }	
                        ?>
                    </div>
                </div>
            </div>
       </div>
	</section>
	<aside>
		<nav>
			<ul>
				<li class="title <?php if(!isset($_GET['p']) && !isset($_POST['search']) || $_GET['p'] == 't'){echo 'active';} ?>"><a href="index.php?p=t">Timeline</a></li>
                <li class="title <?php if((isset($_GET['p']) && $_GET['p'] == 'r') || isset($_POST['search']) || isset($_GET['s'])){echo 'active';} ?>"><a href="index.php?p=r">Recherches</a></li>
				<li class="title <?php if(isset($_GET['p']) && $_GET['p'] == 'm'){echo 'active';} ?>"><a href="index.php?p=m">@<?php echo $_SESSION['cleverUser']['user_name']; ?></a></li>
			</ul>
		</nav>
        <div id="move">
			<a href="#" class="glisser_droite"></a>
		</div>
		<ul class="twitter">
        	<?php 
				if(isset($_GET['p']) && $_GET['p'] == 'm') {
					$cleverConnect->toMentions($_SESSION['cleverUser']['acess_token'], $_SESSION['cleverUser']['acess_token_secret']);
				}
				elseif(isset($_POST['search']) && !empty($_POST['search'])) {
					$_SESSION['cleverUser']['save_search'] = $cleverTimeSearch = $hashs; 
					$cleverSearch->toTimeSearch($_SESSION['cleverUser']['user_name'],$cleverTimeSearch);
				}
				elseif(isset($_GET['p']) && $_GET['p'] == 'r') { 
					$search_clv = array();
					$search_clv[0] = $_SESSION['cleverUser']['save_search'];
					$cleverSearch->toTimeSearch($_SESSION['cleverUser']['user_name'],$search_clv);
				}
				elseif(isset($_GET['p']) && $_GET['p'] == 'r' && isset($_REQUEST['s']) && !empty($_REQUEST['s'])) { 
					$search_clv = array();
					$search_clv[0] = $_REQUEST['s'];
					$cleverSearch->toTimeSearch($_SESSION['cleverUser']['user_name'],$search_clv);
				}
				elseif(isset($_GET['p']) && $_GET['p'] == 't') { 
					$cleverConnect->toTimeline($_SESSION['cleverUser']['acess_token'],$_SESSION['cleverUser']['acess_token_secret']);
				}
				else {
					$cleverConnect->toTimeline($_SESSION['cleverUser']['acess_token'], $_SESSION['cleverUser']['acess_token_secret']);
				}
			?>
		</ul>
	</aside>