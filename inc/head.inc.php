<?php	
	/* ---------------------------------------------------- */
	//Déconnexion
	/* ---------------------------------------------------- */
	if(isset($_GET['p']) && $_GET['p'] == 'd')
	{
		$cleverConnect = new cleverConnect();
		$cleverConnect->toDeconnect();
	}
?>
<!Doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>CleverBird, l'oiseau intelligent qui aide à mieux tweeter.</title>

<link rel="shortcut icon" href="images/redball.png" type="image/x-icon" />
<!-- CSS -->
<link rel="stylesheet" href="css/reset.css" type="text/css" media="all" />
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
<!-- CSS -->

<!-- META -->
<meta name="description" content="Venez tester CleverBird, l'outil intelligent qui va trouver pour vous les tendances Twitter dans les domaines que vous choisissez!" /> 
<!-- META -->

<!-- JS -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js" type="text/javascript"></script>
<script src="js/start.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery-1.2.6.min.js"></script>
<script type="text/javascript" src="js/jquery.jparallax.js"></script>
<script type="text/javascript">
<!--
// jQuery.noConflict();
// RUN
$(document).ready(function(){	
	$('#parallax').jparallax({triggerExposesEdges: true}).append(corners);
});
//-->
</script>
<!-- JS -->

</head>
<body>
<header>
		<a href="index.php" id="logo">CleverBird <h1>Les tendances actuelles vous permettent d'améliorer votre notoriété sur Twitter!</h1></a>
