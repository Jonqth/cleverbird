
$.noConflict();
  jQuery(document).ready(function($) {
	
	/* G�n�ral */
	
	var reglages = function(){
		var height = document.documentElement.clientHeight;
		var width = document.documentElement.clientWidth;
		$("section").css("height",height - 51);
		$("aside ul.twitter").css("height",height - 96);
		$(".background").css("height",height - 128);
		$("#page #content").css("height",height - 74);
	}
	
	reglages();
	
	$(window).resize(function() {
		reglages();
   });
   
   /* Form */
   
   $("#rechercher").live('click',function(){
		$('#board nav form textarea').attr('name','search');
   });
   $("#tweeter").live('click',function(){
		$('#board nav form textarea').attr('name','tweet');
   });
   
   /* Slider */
   
   var slider = function(){
		$('img1, img.11, img.111, img.1111').hide();
		$('img.1').show().delay(7000).fadeOut(800);
		$('img.11').hide().delay(7800).fadeIn(800).delay(7000).fadeOut(800);
		$('img.111').hide().delay(15600).fadeIn(800).delay(7000).fadeOut(800);
		$('img.1111').hide().delay(24000).fadeIn(800);
	}
	
	slider();
   
    /* Bulles */
	
	var taille_bulle = function(){
		var width = $('#bulle a').width;
		$("#bulle").css("height",width);
		$("#bulle").css("width",width);
	}
	
	taille_bulle();
	
	/* D�pliant */
	
	$(".glisser_droite").live('click',function(){
		$("aside").animate({
			right: '-30%'
		  }, 1000, function() {
			// Animation complete.
		  });
		$("section, .background, footer").animate({
			width : '100%'
		  }, 1000, function() {
			$('aside #move a').removeClass('glisser_droite');
			$('aside #move a').addClass('glisser_gauche');
		  });
		$('aside #move a').css("background","url(images/fleche_left.png) no-repeat");
		  return false;
	});
	
	$(".glisser_gauche").live('click', function(){
		$("aside").animate({
			right: '0%'
		  }, 1000, function() {
			// Animation complete.
		  });
		$("footer").animate({
			width : '69.6%'
			}, 1000, function() {
			// Animation complete.
		  });
		$("section, .background").animate({
			width : '70%'
		  }, 1000, function() {
			$('aside #move a').removeClass('glisser_gauche');
			$('aside #move a').addClass('glisser_droite');
		  });
		$('aside #move a').css("background","url(images/fleche_right.png) no-repeat");
		  return false;
	});;
   
 });
 